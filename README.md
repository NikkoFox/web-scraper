# Web Scraper

[![Hits-of-Code](https://hitsofcode.com/gitlab/NikkoFox/web-scraper)](https://hitsofcode.com/gitlab/NikkoFox/web-scraper/view)

## Задача

Создать программу поисковик (консольную) Пользователь вводит текст запроса, поисковую систему (google.com, yandex.ru, ...), количество результатов, рекурсивный поиск или нет, формат вывода (в консоль, в файл json, в csv) Программа находит в интернете начиная от стартовой точки все ссылки на веб-странице в заданном количестве (название ссылки и саму ссылку) Если поиск не рекурсивный, то берем ссылки только из поисковика, если рекурсивный, то берем первую ссылку, переходим, находим там ссылки, переходим, ... В зависимости от выбранного формата вывода сохраняем результат (текст ссылки: ссылка) либо в консоль либо в файл выбранного формата

## Установка 

`pip install .`

## Использование

Для использования selenium драйвера, необходимо, установить браузер FireFox и geckodriver.

```
usage: cli_crawler.py [-h] [-d {selenium,requests}] [-e {duckduckgo,google}] [-q QUANTITY] [-r] [-c | -j | -s] query [query ...]

Collecting links for a search query and saving the result in various formats

positional arguments:
  query                 search query

optional arguments:
  -h, --help            show this help message and exit
  -d {selenium,requests}, --driver {selenium,requests}
                        web scraper driver
  -e {duckduckgo,google}, --engine {duckduckgo,google}
                        search engine
  -q QUANTITY, --quantity QUANTITY
                        number of links
  -r, --recursive       search links recursively
  -c, --csv             output of the result in the .csv file
  -j, --json            output of the result in the .json file
  -s, --sqlite          output of the result in the sqlite database


```
