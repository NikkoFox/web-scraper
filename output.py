import csv
import json
import os
import sqlite3
from abc import abstractmethod
from uuid import uuid4


class Output:
    def __init__(self, filename: str = None):
        self.data = []
        self.filename = filename.replace(' ', '_') if filename else str(uuid4())

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.write()

    def __len__(self):
        return len(self.data)

    def add(self, text: str, link: str):
        self.data.append({'text': text.replace('\n', ''), 'link': link.replace('\n', '')})

    @abstractmethod
    def write(self):
        pass

    @property
    def fieldnames(self) -> tuple:
        return 'text', 'link'


class JSON(Output):
    def write(self):
        path = os.path.join(os.getcwd(), f"{self.filename}.json")
        with open(path, "w") as f:
            json.dump(self.data, f, indent=2, ensure_ascii=False)


class CSV(Output):
    def write(self):
        path = os.path.join(os.getcwd(), f"{self.filename}.csv")
        with open(path, "w") as f:
            wr = csv.DictWriter(f, delimiter=';', fieldnames=self.fieldnames)
            wr.writeheader()
            wr.writerows(self.data)


class SQLite(Output):
    def _connect(self):
        sql = ("CREATE TABLE IF NOT EXISTS `search_result` ("
               "`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, "
               "`text`	TEXT, "
               "`link`	TEXT);")
        db_file = os.path.join(os.getcwd(), f"{self.filename}.sqlite")
        connect = sqlite3.connect(db_file)
        with connect:
            cursor = connect.cursor()
            cursor.executescript(sql)
            cursor.close()
        return connect

    def write(self):
        with self._connect() as db:
            cursor = db.cursor()
            cursor.executemany("INSERT INTO search_result (text, link)"
                               "VALUES (:text, :link);", self.data)
            db.commit()
            cursor.close()


class Console(Output):
    def __init__(self, continuously_write: bool = False):
        super().__init__()
        if continuously_write:
            self.add = self.__add
        self.continuously_write = continuously_write

    def write(self):
        if not self.continuously_write:
            print('\n'.join([f'{x.get(self.fieldnames[0])}: {x.get(self.fieldnames[1])}' for x in self.data]))

    def __add(self, text: str, link: str):
        super().add(text, link)
        print("{}: {}".format(text.replace('\n', ''), link.replace('\n', '')))
