import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="Web-Scraper",
    version="0.1.0",
    author="Artyom Sviridov",
    description="Collecting links for a search query and saving the result in various formats",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/NikkoFox/web-scraper",
    packages=setuptools.find_packages(),
    py_modules=['cli_crawler', 'output', 'scraper'],
    include_package_data=True,
    python_requires=">=3.7",
    install_requires=[
        "beautifulsoup4==4.9.3",
        "certifi==2020.12.5",
        "chardet==4.0.0",
        "flake8==3.9.1",
        "idna==2.10",
        "lxml==4.6.3",
        "mccabe==0.6.1",
        "pycodestyle==2.7.0",
        "pyflakes==2.3.1",
        "requests==2.25.1",
        "selenium==3.141.0",
        "soupsieve==2.2.1",
        "urllib3==1.26.4",
    ],
    entry_points={
        'console_scripts': [
            'scraper = cli_crawler:main',
        ]
    }
)
