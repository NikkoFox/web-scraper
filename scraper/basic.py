from abc import abstractmethod
from enum import Enum

from output import Output, Console


class BasicScraper:
    def __init__(self, query: str, quantity: int, recursive: bool = False, output: Output = None):
        self.query = query
        self.quantity = quantity
        self.recursive = recursive
        self.output = output if output is not None else Console()
        self.unique_links = set()

    def search(self):
        while self.quantity > len(self.output):
            self.find_links()
            self.next_page()

    @abstractmethod
    def find_links_from_url(self, url: str):
        pass

    @abstractmethod
    def find_links(self):
        pass

    @abstractmethod
    def next_page(self):
        pass

    def _is_link_correct(self, link: str) -> bool:
        return link.startswith('http') and link not in self.unique_links


class SupportedEngine(Enum):
    duckduckgo = 'https://html.duckduckgo.com/html/'
    google = 'https://www.google.com/search'
