import time
from abc import abstractmethod
from random import uniform

import requests
from bs4 import BeautifulSoup
from lxml import html
from requests import Response

from output import Output
from scraper.basic import BasicScraper, SupportedEngine


class Requests(BasicScraper):
    def __init__(self, query: str, quantity: int, recursive: bool = False, output: Output = None):
        super().__init__(query, quantity, recursive, output)
        self.headers = {'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) '
                                      'AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36'}
        self.extra_params = dict()

    def get(self, url: str, data: dict = None):
        time.sleep(uniform(1.0, 3.0))
        return requests.get(url, params=data, headers=self.headers)

    @staticmethod
    def get_lxml_tree(response: Response):
        if response.status_code != 200:
            raise Exception(response.reason)
        return html.fromstring(response.content)

    @staticmethod
    def get_soup(response: Response):
        if response.status_code != 200:
            raise Exception(response.reason)
        return BeautifulSoup(response.content, 'lxml')

    def find_links_from_url(self, url: str):
        try:
            r = self.get(url)
            soup = self.get_soup(r)
        except Exception:
            return
        self.__find_all_tag_a(soup)

    def __find_all_tag_a(self, soup: BeautifulSoup):
        for a in soup.find_all("a"):
            if len(self.output) == self.quantity:
                break

            current_link = a.get("href", '')
            if self._is_link_correct(current_link) and a.text:
                self.output.add(a.text, current_link)
                self.unique_links.add(current_link)
                self.find_links_from_url(current_link)

    @abstractmethod
    def find_links(self):
        pass

    @abstractmethod
    def next_page(self):
        pass


class GoogleRequests(Requests):
    def next_page(self):
        if 'start' not in self.extra_params:
            self.extra_params['start'] = 10
        else:
            self.extra_params['start'] += 10

    def find_links(self):
        r = self.get(SupportedEngine.google.value, data={**self.extra_params, 'q': self.query})
        soup = self.get_soup(r)

        for g_class in soup.find_all(class_='g'):
            if self.quantity == len(self.output):
                break

            a = g_class.find("a")
            if a.get("href") and a.find("h3"):
                self.output.add(a.find("h3").text, a.get("href"))
                if self.recursive:
                    self.find_links_from_url(a.get("href"))


class DuckDuckGoRequests(Requests):
    def __init__(self, query: str, quantity: int, recursive: bool = False, output: Output = None):
        super().__init__(query, quantity, recursive, output)
        self.hidden_input = None

    def next_page(self):
        self.extra_params = {el.get('name'): el.get('value') for el in self.hidden_input or []}

    def find_links(self):
        r = self.get(SupportedEngine.duckduckgo.value, data={**self.extra_params, 'q': self.query})
        tree = self.get_lxml_tree(r)

        for a in tree.xpath("//h2/a[contains(@class, 'result__a')]"):
            if self.quantity == len(self.output):
                break

            if a.get("href"):
                self.output.add(a.text, a.get("href"))
                if self.recursive:
                    self.find_links_from_url(a.get("href"))

        self.hidden_input = tree.xpath("//div[contains(@class, 'nav-link')]"
                                       "[last()]/form/input[contains(@type, 'hidden')]")
